--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Fri Feb 14 12:31:03 2020
--Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    S : out STD_LOGIC_VECTOR ( 39 downto 0 )
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=12,numReposBlks=12,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=5,da_ps7_cnt=1,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_D_flipflop_0_0 is
  port (
    clk : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_D_flipflop_0_0;
  component design_1_D_flipflop_0_1 is
  port (
    clk : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_D_flipflop_0_1;
  component design_1_c_addsub_0_1 is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 32 downto 0 )
  );
  end component design_1_c_addsub_0_1;
  component design_1_cordic_0_0 is
  port (
    aclk : in STD_LOGIC;
    s_axis_cartesian_tvalid : in STD_LOGIC;
    s_axis_cartesian_tdata : in STD_LOGIC_VECTOR ( 39 downto 0 );
    m_axis_dout_tvalid : out STD_LOGIC;
    m_axis_dout_tdata : out STD_LOGIC_VECTOR ( 39 downto 0 )
  );
  end component design_1_cordic_0_0;
  component design_1_sim_clk_gen_0_0 is
  port (
    clk : out STD_LOGIC;
    sync_rst : out STD_LOGIC
  );
  end component design_1_sim_clk_gen_0_0;
  component design_1_xadc_wiz_0_0 is
  port (
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tid : out STD_LOGIC_VECTOR ( 4 downto 0 );
    m_axis_aclk : in STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    m_axis_resetn : in STD_LOGIC;
    vp_in : in STD_LOGIC;
    vn_in : in STD_LOGIC;
    channel_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    eoc_out : out STD_LOGIC;
    alarm_out : out STD_LOGIC;
    eos_out : out STD_LOGIC;
    busy_out : out STD_LOGIC
  );
  end component design_1_xadc_wiz_0_0;
  component design_1_xbip_dsp48_macro_0_0 is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    B : in STD_LOGIC_VECTOR ( 15 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_xbip_dsp48_macro_0_0;
  component design_1_xbip_dsp48_macro_0_1 is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    B : in STD_LOGIC_VECTOR ( 15 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_xbip_dsp48_macro_0_1;
  component design_1_xfft_0_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_config_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_config_tvalid : in STD_LOGIC;
    s_axis_config_tready : out STD_LOGIC;
    s_axis_data_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_data_tvalid : in STD_LOGIC;
    s_axis_data_tready : out STD_LOGIC;
    s_axis_data_tlast : in STD_LOGIC;
    m_axis_data_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_data_tvalid : out STD_LOGIC;
    m_axis_data_tready : in STD_LOGIC;
    m_axis_data_tlast : out STD_LOGIC;
    event_frame_started : out STD_LOGIC;
    event_tlast_unexpected : out STD_LOGIC;
    event_tlast_missing : out STD_LOGIC;
    event_status_channel_halt : out STD_LOGIC;
    event_data_in_channel_halt : out STD_LOGIC;
    event_data_out_channel_halt : out STD_LOGIC
  );
  end component design_1_xfft_0_0;
  component design_1_xlconstant_0_0 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_xlconstant_0_0;
  component design_1_xlslice_0_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_1_xlslice_0_0;
  component design_1_xlslice_1_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_1_xlslice_1_0;
  signal D_flipflop_0_Q : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal D_flipflop_1_Q : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Net : STD_LOGIC;
  signal c_addsub_0_S : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal cordic_0_m_axis_dout_tdata : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal sim_clk_gen_0_sync_rst : STD_LOGIC;
  signal xadc_wiz_0_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xadc_wiz_0_M_AXIS_TREADY : STD_LOGIC;
  signal xadc_wiz_0_M_AXIS_TVALID : STD_LOGIC;
  signal xbip_dsp48_macro_0_P : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbip_dsp48_macro_1_P : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xfft_0_m_axis_data_tdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xlconstant_0_dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlslice_0_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xlslice_1_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_cordic_0_m_axis_dout_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_cordic_0_s_axis_cartesian_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 39 downto 33 );
  signal NLW_xadc_wiz_0_alarm_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_busy_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_eoc_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_eos_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_channel_out_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_xadc_wiz_0_m_axis_tid_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_xfft_0_event_data_in_channel_halt_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_event_data_out_channel_halt_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_event_frame_started_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_event_status_channel_halt_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_event_tlast_missing_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_event_tlast_unexpected_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_m_axis_data_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_m_axis_data_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_xfft_0_s_axis_config_tready_UNCONNECTED : STD_LOGIC;
begin
  S(39 downto 0) <= cordic_0_m_axis_dout_tdata(39 downto 0);
D_flipflop_0: component design_1_D_flipflop_0_0
     port map (
      D(31 downto 0) => xbip_dsp48_macro_0_P(31 downto 0),
      Q(31 downto 0) => D_flipflop_0_Q(31 downto 0),
      clk => Net
    );
D_flipflop_1: component design_1_D_flipflop_0_1
     port map (
      D(31 downto 0) => xbip_dsp48_macro_1_P(31 downto 0),
      Q(31 downto 0) => D_flipflop_1_Q(31 downto 0),
      clk => Net
    );
c_addsub_0: component design_1_c_addsub_0_1
     port map (
      A(31 downto 0) => D_flipflop_0_Q(31 downto 0),
      B(31 downto 0) => D_flipflop_1_Q(31 downto 0),
      CLK => Net,
      S(32 downto 0) => c_addsub_0_S(32 downto 0)
    );
cordic_0: component design_1_cordic_0_0
     port map (
      aclk => Net,
      m_axis_dout_tdata(39 downto 0) => cordic_0_m_axis_dout_tdata(39 downto 0),
      m_axis_dout_tvalid => NLW_cordic_0_m_axis_dout_tvalid_UNCONNECTED,
      s_axis_cartesian_tdata(39 downto 33) => NLW_cordic_0_s_axis_cartesian_tdata_UNCONNECTED(39 downto 33),
      s_axis_cartesian_tdata(32 downto 0) => c_addsub_0_S(32 downto 0),
      s_axis_cartesian_tvalid => xlconstant_0_dout(0)
    );
sim_clk_gen_0: component design_1_sim_clk_gen_0_0
     port map (
      clk => Net,
      sync_rst => sim_clk_gen_0_sync_rst
    );
xadc_wiz_0: component design_1_xadc_wiz_0_0
     port map (
      alarm_out => NLW_xadc_wiz_0_alarm_out_UNCONNECTED,
      busy_out => NLW_xadc_wiz_0_busy_out_UNCONNECTED,
      channel_out(4 downto 0) => NLW_xadc_wiz_0_channel_out_UNCONNECTED(4 downto 0),
      eoc_out => NLW_xadc_wiz_0_eoc_out_UNCONNECTED,
      eos_out => NLW_xadc_wiz_0_eos_out_UNCONNECTED,
      m_axis_aclk => Net,
      m_axis_resetn => sim_clk_gen_0_sync_rst,
      m_axis_tdata(15 downto 0) => xadc_wiz_0_M_AXIS_TDATA(15 downto 0),
      m_axis_tid(4 downto 0) => NLW_xadc_wiz_0_m_axis_tid_UNCONNECTED(4 downto 0),
      m_axis_tready => xadc_wiz_0_M_AXIS_TREADY,
      m_axis_tvalid => xadc_wiz_0_M_AXIS_TVALID,
      s_axis_aclk => Net,
      vn_in => '0',
      vp_in => '0'
    );
xbip_dsp48_macro_0: component design_1_xbip_dsp48_macro_0_0
     port map (
      A(15 downto 0) => xlslice_0_Dout(15 downto 0),
      B(15 downto 0) => xlslice_0_Dout(15 downto 0),
      CLK => Net,
      P(31 downto 0) => xbip_dsp48_macro_0_P(31 downto 0)
    );
xbip_dsp48_macro_1: component design_1_xbip_dsp48_macro_0_1
     port map (
      A(15 downto 0) => xlslice_1_Dout(15 downto 0),
      B(15 downto 0) => xlslice_1_Dout(15 downto 0),
      CLK => Net,
      P(31 downto 0) => xbip_dsp48_macro_1_P(31 downto 0)
    );
xfft_0: component design_1_xfft_0_0
     port map (
      aclk => Net,
      aresetn => sim_clk_gen_0_sync_rst,
      event_data_in_channel_halt => NLW_xfft_0_event_data_in_channel_halt_UNCONNECTED,
      event_data_out_channel_halt => NLW_xfft_0_event_data_out_channel_halt_UNCONNECTED,
      event_frame_started => NLW_xfft_0_event_frame_started_UNCONNECTED,
      event_status_channel_halt => NLW_xfft_0_event_status_channel_halt_UNCONNECTED,
      event_tlast_missing => NLW_xfft_0_event_tlast_missing_UNCONNECTED,
      event_tlast_unexpected => NLW_xfft_0_event_tlast_unexpected_UNCONNECTED,
      m_axis_data_tdata(31 downto 0) => xfft_0_m_axis_data_tdata(31 downto 0),
      m_axis_data_tlast => NLW_xfft_0_m_axis_data_tlast_UNCONNECTED,
      m_axis_data_tready => xlconstant_0_dout(0),
      m_axis_data_tvalid => NLW_xfft_0_m_axis_data_tvalid_UNCONNECTED,
      s_axis_config_tdata(15 downto 0) => B"0000000000000000",
      s_axis_config_tready => NLW_xfft_0_s_axis_config_tready_UNCONNECTED,
      s_axis_config_tvalid => '0',
      s_axis_data_tdata(31 downto 16) => B"0000000000000000",
      s_axis_data_tdata(15 downto 0) => xadc_wiz_0_M_AXIS_TDATA(15 downto 0),
      s_axis_data_tlast => '0',
      s_axis_data_tready => xadc_wiz_0_M_AXIS_TREADY,
      s_axis_data_tvalid => xadc_wiz_0_M_AXIS_TVALID
    );
xlconstant_0: component design_1_xlconstant_0_0
     port map (
      dout(0) => xlconstant_0_dout(0)
    );
xlslice_0: component design_1_xlslice_0_0
     port map (
      Din(31 downto 0) => xfft_0_m_axis_data_tdata(31 downto 0),
      Dout(15 downto 0) => xlslice_0_Dout(15 downto 0)
    );
xlslice_1: component design_1_xlslice_1_0
     port map (
      Din(31 downto 0) => xfft_0_m_axis_data_tdata(31 downto 0),
      Dout(15 downto 0) => xlslice_1_Dout(15 downto 0)
    );
end STRUCTURE;
